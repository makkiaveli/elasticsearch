package com.example.es;

import com.example.kafka.Key;
import com.example.kafka.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class ElasticSearchRepository {

    private final ElasticsearchTemplate template;

    @Autowired
    public ElasticSearchRepository(ElasticsearchTemplate template) {
        this.template = template;
    }

    public void insert(Message message) {
        Key key = message.getKey();
        IndexQuery query = new IndexQuery();
        query.setIndexName(key.getIndex());
        query.setType(key.getType());
        query.setId(key.getId());
        query.setSource(message.getMessage());
        template.bulkIndex(Collections.singletonList(query));
        template.refresh(key.getIndex());
    }
}
