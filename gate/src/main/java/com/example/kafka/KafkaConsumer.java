package com.example.kafka;

import com.example.es.ElasticSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    private final ElasticSearchRepository repository;

    @Autowired
    public KafkaConsumer(ElasticSearchRepository repository) {
        this.repository = repository;
    }

    @KafkaListener(topics = "${kafka.topicName}", groupId = "${kafka.group}")
    public void consume(@Payload String data,
                          @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) Key key) {
        logger.info("received key='{}', message='{}'", key, data);
        Message message = new Message() {{
           setKey(key);
           setMessage(data);
        }};

        repository.insert(message);
    }
}
