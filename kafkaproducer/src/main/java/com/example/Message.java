package com.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Component
public class Message {

    private static Lorem lorem = LoremIpsum.getInstance();
    public Key createKey(long index) {
        Key key = new Key();
        key.setIndex("document");
        key.setType("post");
        key.setId(String.valueOf(index));
        return key;
    }

    public String createMessage() throws JsonProcessingException {
        Map<String, String> object = new HashMap<>();
        object.put("name", lorem.getName());
        object.put("lastName", lorem.getLastName());
        object.put("email", lorem.getEmail());
        object.put("phone", lorem.getPhone());
        object.put("title", lorem.getTitle(Integer.valueOf(randomNumeric(1))));
        object.put("world", lorem.getWords(Integer.valueOf(randomNumeric(2))));
        return new ObjectMapper().writeValueAsString(object);
    }
}
